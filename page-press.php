<?php
/**
 * Template Name: Press Page
 *
 * This is the template that displays all pages by default.
 *
 * @package Starter_Theme
 */

get_header(); ?>

<section id="primary" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <?php the_content(); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'themeTextDomain' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php the_field( 'button_text' ); ?>
                <?php the_field( 'button_url' ); ?>
            </div><!-- .entry-content -->
        </article><!-- #post-<?php the_ID(); ?> -->

    <?php endwhile; // end of the loop. ?>
    <h1>social media icons</h1>
    <div class="social">
        <?php the_field( 'facebook_url', 'options' ); ?>
        <?php the_field( 'twitter_url', 'options' ); ?>
    </div>

</section><!-- #primary -->

<?php get_footer(); ?>