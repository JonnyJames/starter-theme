<?php
/**
 * The footer template
 *
 * Contains the closing of <div id="main"> and all content after.
 *
 * @package Starter_Theme
 */
?>

    </div><!-- #main -->

</div><!-- #page -->
	<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>

	<footer id="colophon" role="contentinfo">
	    <div id="copyright">
	        <!-- copyright goes here -->
	        <?php echo date('Y'); ?> <?php bloginfo('name'); ?><br>
	        <a href="http://jonnyjames.com" rel="nofollow">theme by Jonny James</a>
	    </div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?> 
</body>
</html>