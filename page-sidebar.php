<?php
/**
 * The Page template
 *
 * This is the template that displays all pages by default.
 *
 * @package Starter_Theme
 */

get_header(); ?>

<section id="primary" role="main">

    <!-- Everything in here is the same as the default template shown above -->

</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>